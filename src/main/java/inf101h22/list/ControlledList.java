package inf101h22.list;

import java.util.Iterator;
import java.util.List;

import inf101h22.observable.ObservableCore;

/**
 * A ControlledList is an Observable wrapper around any List. Note
 * that this current simplistic version only implements a small subset
 * of the List methods; for the future it might be better to let it
 * implement the complete List<E> interface.
 * 
 * Another useful property of a ControlledList is the more restrictive
 * interface ObservableList which provides a read-only interface for
 * the list whilst also allowing it to be observed.
 */
public class ControlledList<E> extends ObservableCore implements ObservableList<E> {

    private List<E> list;

    public ControlledList(List<E> underlyingList) {
        this.list = underlyingList;
    }

    @Override
    public Iterator<E> iterator() {
        return this.list.iterator();
    }

    /**
     * Adds an element to the end of the list.
     * 
     * @param item item to add
     * @return true if the operation was successful, false otherwise
     */
    public boolean add(E item) {
        boolean res = this.list.add(item);
        this.notifyObservers();
        return res;
    }

    /**
     * Sets the value at the given position in the list.
     * Returns the previous value in that position.
     *
     * @param index
     * @param item
     * @return
     */
    public E set(int index, E item) {
        E res = this.list.set(index, item);
        this.notifyObservers();
        return res;
    }
    
    /** Removes all elements from the list. */
    public void clear() {
        this.list.clear();
        this.notifyObservers();
    }
    
    @Override
    public E get(int index) {
        return this.list.get(index);
    }
    
    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public int size() {
        return this.list.size();
    }

    public void pop() {
        if (this.size() > 0) this.list.remove(this.list.size() - 1);
        this.notifyObservers();
    }
}
