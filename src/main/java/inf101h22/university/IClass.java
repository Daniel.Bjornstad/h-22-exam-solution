package inf101h22.university;

/**
 * Interface for datatype representing an university class of students.
 */
public interface IClass extends Iterable<Student> {
    
    /**
     * Get the average GPA of all students in the class.
     * @return average GPA of all students
     */
    public double getAverageGpa();

    /**
     * Get the number of students in the class.
     * @return number of students in the class
     */
    public int size();

    /**
     * Adds a student to the class.
     * Always returns true.
     * 
     * @param student
     */
    public boolean add(Student student);

}
