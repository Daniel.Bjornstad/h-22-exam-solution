package inf101h22.treedrawer.model;

import java.util.ArrayList;

import inf101h22.list.ControlledList;
import inf101h22.list.ObservableList;
import inf101h22.observable.ControlledObservable;
import inf101h22.observable.ObservableValue;

/**
 * The TreeCanvas is the main class of the model, containing variables pertaining to the abstract
 * state of the program. The class contains a collection of Node objects, as well as a status
 * message.
 */
public class TreeCanvas implements ReadableTreeCanvas {

    // Constants
    private static final String WELCOME_MSG = "Click anywhere to start drawing";
    private static final String ACTIVE_MSG = "Your tree has %d nodes";

    // Field variables
    private ControlledList<Node> nodes = new ControlledList<>(new ArrayList<>());
    private ControlledObservable<String> message = new ControlledObservable<String>(WELCOME_MSG);

    // This code between curly braces runs when the TreeCanvas object is created
    { this.nodes.addObserver(this::updateMessage); }

    /** Recalculates the message based on the current state. */
    private void updateMessage() {
        if (this.nodes.isEmpty()) {
            this.message.setValue(WELCOME_MSG);
        }
        else {
            this.message.setValue(String.format(ACTIVE_MSG, this.nodes.size()));
        }
    }

    /**
     * Adds a new node to the model
     * @param x x-coordinate of the new node
     * @param y y-coordinate of the new node
     */
    public void addNode(double x, double y) {
        Node neighbor = getNearestNodeTo(x, y);
        this.nodes.add(new Node(x, y, neighbor));
    }

    /** Clears the canvas. */
    public void clear() {
        this.nodes.clear();
    }

    /** Removes last added Node. */
    public void undo() {
        this.nodes.pop();        
    }

    @Override
    public ObservableList<Node> getNodes() {
        return nodes;
    }

    @Override
    public ObservableValue<String> getMessage() {
        return this.message;
    }


    /** Gets the nearest node to the given coordinate, or null if no previous node exist. */
    private Node getNearestNodeTo(double x, double y) {
        Node nearest = null;
        double nearestSquareDistance = 4.0; // On 1x1 canvas, square distance will never be this big.
        for (Node node : this.nodes) {
            double distanceSquared = distanceSquared(x, y, node.x, node.y);
            if (distanceSquared < nearestSquareDistance) {
                nearestSquareDistance = distanceSquared;
                nearest = node;
            }
        }
        return nearest;
    }

    /** Gets the square of the Eucledian distance between to coordinates. */
    private double distanceSquared(double x1, double y1, double x2, double y2) {
        return (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2);
    }
}
