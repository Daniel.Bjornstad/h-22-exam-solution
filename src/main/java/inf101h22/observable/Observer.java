package inf101h22.observable;

public interface Observer {

    /** Called when the observed object is mutated. */
    void update();
    
}
